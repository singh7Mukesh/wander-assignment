package com.wanderassignment.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.wanderassignment.model.offline.Note;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;


@Dao
public interface NoteDao {
    @Query("SELECT * FROM Note order by timeInMiles desc")
    List<Note> getAll();


    @Query("SELECT * FROM Note where id=:id")
    Note get(int id);

    @Insert(onConflict = REPLACE)
    long insert(Note data);

    @Update
    void update(Note repos);

    @Delete
    void delete(Note... data);

}