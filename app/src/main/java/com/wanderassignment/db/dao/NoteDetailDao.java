package com.wanderassignment.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.wanderassignment.model.offline.NoteDetail;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;


@Dao
public interface NoteDetailDao {
    @Query("SELECT * FROM NoteDetail ")
    List<NoteDetail> getAll();


    @Query("SELECT * FROM NoteDetail where noteId=:noteId")
    NoteDetail get(int noteId);

    @Insert(onConflict = REPLACE)
    void insert(NoteDetail data);

    @Update
    void update(NoteDetail repos);

    @Delete
    void delete(NoteDetail... data);

}