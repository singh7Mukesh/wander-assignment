package com.wanderassignment.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.wanderassignment.db.dao.NoteDao;
import com.wanderassignment.db.dao.NoteDetailDao;
import com.wanderassignment.model.offline.NoteDetail;
import com.wanderassignment.model.offline.Note;


@Database(entities = {Note.class, NoteDetail.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract NoteDao getNoteDao();

    public abstract NoteDetailDao getNoteDetailDao();

}