package com.wanderassignment.listener;

public interface OnCreateNewNoteListener {

    void onAddNewNote();
}
