package com.wanderassignment.listener.online;

import com.wanderassignment.model.offline.Note;
import com.wanderassignment.model.online.NoteBaseModel;

public interface OnLiveNoteSaveListener {

    void onDone();

    void onRefreshDoneView(int visibility);

    void onNewNoteAdded(NoteBaseModel note);

    void onNoteUpdated(NoteBaseModel note);

    void onNoteRemove();
}
