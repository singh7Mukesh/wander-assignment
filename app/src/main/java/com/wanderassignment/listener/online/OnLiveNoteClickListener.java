package com.wanderassignment.listener.online;

import com.wanderassignment.model.offline.Note;
import com.wanderassignment.model.online.NoteBaseModel;

public interface OnLiveNoteClickListener {

    void onNoteClick(int position, NoteBaseModel data);

}
