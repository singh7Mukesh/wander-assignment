package com.wanderassignment.listener.offline;

import com.wanderassignment.model.offline.Note;

public interface OnNoteSaveListener {

    void onDone();

    void onRefreshDoneView(int visibility);

    void onNewNoteAdded(Note note);

    void onNoteUpdated(Note note);

    void onNoteRemove();
}
