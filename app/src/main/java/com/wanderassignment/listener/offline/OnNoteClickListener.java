package com.wanderassignment.listener.offline;

import com.wanderassignment.model.offline.Note;

public interface OnNoteClickListener {

    void onNoteClick(int position, Note data);

}
