package com.wanderassignment.dagger.module;

import com.wanderassignment.MainActivity;
import com.wanderassignment.dagger.scope.MyApplicationScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mukesh $ingh on 7/30/2018.
 */
@Module
public class ActivityModule {

    private final MainActivity mainActivity;

    public ActivityModule(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Provides
    @MyApplicationScope
    public MainActivity provideActivity() {
        return mainActivity;
    }
}
