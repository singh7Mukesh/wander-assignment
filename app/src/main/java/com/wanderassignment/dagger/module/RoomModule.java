package com.wanderassignment.dagger.module;

import android.arch.persistence.room.Room;

import com.wanderassignment.constant.AppConstant;
import com.wanderassignment.dagger.scope.MyApplicationScope;
import com.wanderassignment.db.AppDatabase;
import com.wanderassignment.ApplicationSingle;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mukesh $ingh on 7/30/2018.
 */
@Module
public class RoomModule {

    @MyApplicationScope
    @Provides
    @Inject
    public AppDatabase provideAppDatabase(ApplicationSingle application) {
        return Room.databaseBuilder(application,
                AppDatabase.class,
                AppConstant.IAppUtils.DB_NAME)
                .allowMainThreadQueries()
                .build();
    }

}
