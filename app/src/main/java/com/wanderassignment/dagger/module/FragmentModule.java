package com.wanderassignment.dagger.module;

import com.wanderassignment.MainActivity;
import com.wanderassignment.dagger.scope.MyApplicationScope;
import com.wanderassignment.ApplicationSingle;
import com.wanderassignment.support.FlowOrganizer;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mukesh $ingh on 7/30/2018.
 */
@Module
public class FragmentModule {

    @MyApplicationScope
    @Provides
    @Inject
    public FlowOrganizer provideFlowOrganiser(ApplicationSingle application, MainActivity mainActivity) {
        return new FlowOrganizer(application, mainActivity);
    }
}
