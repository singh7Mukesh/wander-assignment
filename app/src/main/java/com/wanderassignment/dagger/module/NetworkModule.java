package com.wanderassignment.dagger.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wanderassignment.apiz.ApiInterface;
import com.wanderassignment.constant.AppConstant;
import com.wanderassignment.dagger.scope.MyApplicationScope;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mukesh $ingh on 8/1/2018.
 */
@Module
public class NetworkModule {

    @Provides
    @MyApplicationScope
    @Inject
    ApiInterface provideApiInterface(Retrofit retroFit) {
        return retroFit.create(ApiInterface.class);
    }

    @Provides
    @MyApplicationScope
    @Inject
    public Retrofit provideRetrofit(OkHttpClient client, Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(AppConstant.IAppUrls.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    @Provides
    @MyApplicationScope
    @Inject
    public OkHttpClient provideClient(HttpLoggingInterceptor interceptor) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(interceptor);
        return httpClient.build();
    }

    @Provides
    @MyApplicationScope
    public HttpLoggingInterceptor provideInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }

    @Provides
    @MyApplicationScope
    public Gson provideGson() {
        return new GsonBuilder()
                .setLenient()
                .create();
    }
}
