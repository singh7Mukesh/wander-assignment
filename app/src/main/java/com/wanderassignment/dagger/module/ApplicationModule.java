package com.wanderassignment.dagger.module;

import com.wanderassignment.dagger.scope.MyApplicationScope;
import com.wanderassignment.ApplicationSingle;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mukesh $ingh on 7/30/2018.
 */
@Module
public class ApplicationModule {

    private final ApplicationSingle application;

    public ApplicationModule(ApplicationSingle application) {
        this.application = application;
    }

    @Provides
    @MyApplicationScope
    public ApplicationSingle provideApplication(){
        return application;
    }
}
