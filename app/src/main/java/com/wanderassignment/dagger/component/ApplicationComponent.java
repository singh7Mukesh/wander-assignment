package com.wanderassignment.dagger.component;

import com.wanderassignment.MainActivity;
import com.wanderassignment.dagger.module.ActivityModule;
import com.wanderassignment.dagger.module.ApplicationModule;
import com.wanderassignment.dagger.module.FragmentModule;
import com.wanderassignment.dagger.module.NetworkModule;
import com.wanderassignment.dagger.module.RoomModule;
import com.wanderassignment.dagger.scope.MyApplicationScope;
import com.wanderassignment.frgment.BaseFragment;
import com.wanderassignment.frgment.FragmentEditNote;
import com.wanderassignment.frgment.FragmentHome;
import com.wanderassignment.ApplicationSingle;
import com.wanderassignment.viewModel.offline.NoteListViewModel;
import com.wanderassignment.viewModel.offline.NoteViewModel;
import com.wanderassignment.viewModel.online.NoteOnlineListViewModel;
import com.wanderassignment.viewModel.online.NoteOnlineViewModel;

import dagger.Component;

/**
 * Created by Mukesh $ingh on 7/30/2018.
 */
@MyApplicationScope
@Component(modules = {ApplicationModule.class,
        ActivityModule.class,
        NetworkModule.class,
        RoomModule.class,
        FragmentModule.class})
public interface ApplicationComponent {

    void inject(BaseFragment baseFragment);

    void inject(FragmentHome fragmentHome);

    void inject(FragmentEditNote fragmentEditNote);

    void inject(MainActivity mainActivity);

    void inject(ApplicationSingle application);

    void inject(NoteListViewModel noteListViewModel);

    void inject(NoteViewModel noteViewModel);

    void inject(NoteOnlineViewModel noteOnlineViewModel);

    void inject(NoteOnlineListViewModel noteOnlineListViewModel);

}
