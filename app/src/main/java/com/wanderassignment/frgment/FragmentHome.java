package com.wanderassignment.frgment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wanderassignment.MainActivity;
import com.wanderassignment.R;
import com.wanderassignment.adapter.AdapterNoteGrid;
import com.wanderassignment.custom.RecyclerViewMargin;
import com.wanderassignment.databinding.FragmentHomeBinding;
import com.wanderassignment.listener.OnCreateNewNoteListener;
import com.wanderassignment.listener.offline.OnNoteClickListener;
import com.wanderassignment.model.offline.Note;
import com.wanderassignment.ApplicationSingle;
import com.wanderassignment.support.FlowOrganizer;
import com.wanderassignment.viewModel.offline.NoteListViewModel;
import com.wanderassignment.viewModel.offline.NoteListViewModelFactory;

import java.util.ArrayList;

import javax.inject.Inject;


public class FragmentHome extends BaseFragment implements OnCreateNewNoteListener {

    private FragmentHomeBinding binding;
    private AdapterNoteGrid adapterGrid;
    private int listMargin;
    public NoteListViewModel noteListViewModel;
    @Inject
    public MainActivity mainActivity;
    @Inject
    public FlowOrganizer flowOrganizer;
    @Inject
    public ApplicationSingle applicationSingle;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        ((ApplicationSingle) getActivity().getApplication()).getApplicationComponent().inject(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        noteListViewModel = ViewModelProviders.of(mainActivity, new NoteListViewModelFactory(applicationSingle))
                .get(NoteListViewModel.class);

        noteListViewModel.getListMutableLiveData().observe(this, new Observer<ArrayList<Note>>() {
            @Override
            public void onChanged(@Nullable ArrayList<Note> notes) {
                if (notes == null || notes.isEmpty()) {
                    binding.recyclerView.setVisibility(View.GONE);
                    binding.txtViewNoNote.setVisibility(View.VISIBLE);
                    adapterGrid = null;
                    binding.recyclerView.setAdapter(null);
                    return;
                }
                binding.recyclerView.setVisibility(View.VISIBLE);
                binding.txtViewNoNote.setVisibility(View.GONE);
                adapterGrid = new AdapterNoteGrid(notes);
                binding.recyclerView.setAdapter(adapterGrid);
                adapterGrid.setOnClickListener(noteListener);
            }
        });
        binding.setViewModel(noteListViewModel);
        noteListViewModel.registerCallBack(this);

        listMargin = mainActivity.getResources().getDimensionPixelOffset(R.dimen._5dp);
        binding.includeActionbar.txtViewTitle.setText(getString(R.string.notes));

        binding.recyclerView.setLayoutManager(new GridLayoutManager(mainActivity,
                2));
        RecyclerViewMargin decoration = new RecyclerViewMargin(listMargin, 2);
        binding.recyclerView.addItemDecoration(decoration);

    }

    private OnNoteClickListener noteListener = new OnNoteClickListener() {
        @Override
        public void onNoteClick(int position, Note data) {
            noteListViewModel.select(data);
            noteListViewModel.setSelectedNotePosition(position);
            flowOrganizer.add(new FragmentEditNote(), true);
        }
    };

    @Override
    public void onAddNewNote() {
        noteListViewModel.setSelectedNotePosition(-1);
        noteListViewModel.select(null);
        flowOrganizer.add(new FragmentEditNote(), true);
    }
}
