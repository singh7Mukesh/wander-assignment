package com.wanderassignment.frgment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wanderassignment.MainActivity;
import com.wanderassignment.R;
import com.wanderassignment.databinding.FragmentEditNoteBinding;
import com.wanderassignment.db.AppDatabase;
import com.wanderassignment.listener.offline.OnNoteSaveListener;
import com.wanderassignment.model.offline.Note;
import com.wanderassignment.model.offline.NoteDetail;
import com.wanderassignment.ApplicationSingle;
import com.wanderassignment.support.FlowOrganizer;
import com.wanderassignment.viewModel.offline.NoteListViewModel;
import com.wanderassignment.viewModel.offline.NoteListViewModelFactory;
import com.wanderassignment.viewModel.offline.NoteViewModel;
import com.wanderassignment.viewModel.offline.NoteViewModelFactory;

import java.util.Calendar;

import javax.inject.Inject;


public class FragmentEditNote extends BaseFragment implements OnNoteSaveListener {

    private FragmentEditNoteBinding binding;
    private NoteViewModel viewModel;
    private int noteId;
    private NoteListViewModel listViewModel;
    @Inject
    public AppDatabase database;
    @Inject
    public MainActivity mainActivity;
    @Inject
    public FlowOrganizer flowOrganizer;
    @Inject
    public ApplicationSingle applicationSingle;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_note, container, false);
        ((ApplicationSingle) getActivity().getApplication()).getApplicationComponent().inject(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = ViewModelProviders.of(this, new NoteViewModelFactory(applicationSingle))
                .get(NoteViewModel.class);
        viewModel.registerCallBack(this);
        binding.setViewModel(viewModel);
        listViewModel = ViewModelProviders.of(mainActivity, new NoteListViewModelFactory(applicationSingle))
                .get(NoteListViewModel.class);
        listViewModel.getSelected().observe(this, new Observer<Note>() {
            @Override
            public void onChanged(@Nullable Note note) {
                if (note != null) {
                    noteId = note.getId();
                    binding.setNote(note);
                    NoteDetail noteDetail = database
                            .getNoteDetailDao().get(noteId);
                    binding.setNoteDetail(noteDetail);
                    viewModel.init(note, noteDetail);
                    hideSoftKeyboard();
                } else {
                    binding.btnTxtViewDone.setVisibility(View.GONE);
                    binding.edtTxtTitle.requestFocus();
                    Note newNote = new Note();
                    newNote.setTimeInMiles(Calendar.getInstance().getTimeInMillis());
                    binding.setNote(newNote);
                    showSoftKeyboard();
                }
            }
        });
    }


    @Override
    public void onPause() {
        super.onPause();
        if (viewModel != null)
            viewModel.saveNote(noteId);
    }

    @Override
    public void onDone() {
        flowOrganizer.popUpBackToMain();
    }

    @Override
    public void onNewNoteAdded(Note note) {
        listViewModel.updateMutableListData(note);
    }

    @Override
    public void onNoteRemove() {
        listViewModel.removeNote();
    }

    @Override
    public void onNoteUpdated(Note note) {
        listViewModel.updateMutableListData(note);
    }

    @Override
    public void onRefreshDoneView(int visibility) {
        binding.btnTxtViewDone.setVisibility(visibility);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        hideSoftKeyboard();
    }
}
