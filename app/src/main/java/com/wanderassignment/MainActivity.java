package com.wanderassignment;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.wanderassignment.databinding.ActivityMainBinding;
import com.wanderassignment.frgment.FragmentHome;
import com.wanderassignment.support.FlowOrganizer;

import javax.inject.Inject;

/**
 * Created by Mukesh on 24-07-2018.
 */


public class MainActivity extends FragmentActivity {

    ActivityMainBinding binding;
    @Inject
    FlowOrganizer flowOrganizer;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((ApplicationSingle) getApplication()).init(this);
        ((ApplicationSingle) getApplication()).getApplicationComponent()
                .inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        flowOrganizer.initParentFrame(binding.frameParent);
        flowOrganizer.add(new FragmentHome());
    }

    public void loading(boolean isLoading) {
        if (isLoading) {
            if (progressDialog == null)
                progressDialog = new ProgressDialog(this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setTitle("Wait");
            progressDialog.setMessage("Loading.........");
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFD4D9D0")));
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        } else {
            if (progressDialog != null)
                progressDialog.dismiss();
        }

    }
}
