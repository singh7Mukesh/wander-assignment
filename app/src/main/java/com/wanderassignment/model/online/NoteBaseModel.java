package com.wanderassignment.model.online;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NoteBaseModel extends BaseModel {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("shotDescription")
    @Expose
    private String shotDescription;
    @SerializedName("timeInMiles")
    @Expose
    private long timeInMiles;
    @SerializedName("noteDetail")
    @Expose
    private String noteDetail;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShotDescription() {
        return shotDescription;
    }

    public void setShotDescription(String shotDescription) {
        this.shotDescription = shotDescription;
    }

    public long getTimeInMiles() {
        return timeInMiles;
    }

    public void setTimeInMiles(long timeInMiles) {
        this.timeInMiles = timeInMiles;
    }

    public String getNoteDetail() {
        return noteDetail;
    }

    public void setNoteDetail(String noteDetail) {
        this.noteDetail = noteDetail;
    }
}
