package com.wanderassignment.model.online;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NoteResponse extends BaseModel {
    @SerializedName("Data")
    @Expose
    private List<NoteBaseModel> listNotes;


    public List<NoteBaseModel> getListNotes() {
        return listNotes;
    }

    public void setListNotes(List<NoteBaseModel> listNotes) {
        this.listNotes = listNotes;
    }
}
