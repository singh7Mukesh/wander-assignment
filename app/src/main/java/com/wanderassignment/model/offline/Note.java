package com.wanderassignment.model.offline;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.wanderassignment.support.Utils;

@Entity
public class Note {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String title;
    private String shotDescription;
    private long timeInMiles;
    private boolean isChecked;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShotDescription() {
        return shotDescription;
    }

    public void setShotDescription(String shotDescription) {
        this.shotDescription = shotDescription;
    }

    public long getTimeInMiles() {
        return timeInMiles;
    }

    public void setTimeInMiles(long timeInMiles) {
        this.timeInMiles = timeInMiles;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getShortDate() {
        return Utils.getShortDate(timeInMiles);
    }


    public String getFullDate() {
        return Utils.getFullDate(timeInMiles);
    }
}
