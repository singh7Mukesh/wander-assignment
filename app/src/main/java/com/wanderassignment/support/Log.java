package com.wanderassignment.support;

/**
 * Created by Mukesh on 24-07-2018.
 */

public class Log {

    private static boolean ENABLE_DEBUG = true;

    public static void e(String tag, String message) {
        if (ENABLE_DEBUG)
            android.util.Log.e(tag, message);
    }

    public static void d(String tag, String message) {
        if (ENABLE_DEBUG)
            android.util.Log.d(tag, message);
    }

    public static void i(String tag, String message) {
        if (ENABLE_DEBUG)
            android.util.Log.i(tag, message);
    }

    public static void v(String tag, String message) {
        if (ENABLE_DEBUG)
            android.util.Log.v(tag, message);
    }

    public static void w(String tag, String message) {
        if (ENABLE_DEBUG)
            android.util.Log.w(tag, message);
    }
}
