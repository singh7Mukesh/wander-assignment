package com.wanderassignment.support;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.wanderassignment.constant.AppConstant.IAppUtils.TIME_FORMAT_LONG;
import static com.wanderassignment.constant.AppConstant.IAppUtils.TIME_FORMAT_SHORT;

public class Utils {

    public static String getFullDate(long milliSeconds) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(TIME_FORMAT_LONG);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }


    public static String getShortDate(long milliSeconds) {
        SimpleDateFormat formatter = new SimpleDateFormat(TIME_FORMAT_SHORT);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }
}
