package com.wanderassignment.support;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.wanderassignment.ApplicationSingle;
import com.wanderassignment.MainActivity;


/**
 * Created by Mukesh on 24-07-2018.
 */

public class FlowOrganizer {

    private FrameLayout frameParent;

    public ApplicationSingle applicationSingle;

    public MainActivity mainActivity;

    public FlowOrganizer(ApplicationSingle applicationSingle, MainActivity mainActivity) {
        this.applicationSingle = applicationSingle;
        this.mainActivity = mainActivity;
    }


    public void initParentFrame(FrameLayout frameParent) {
        this.frameParent = frameParent;
    }

    public void add(Fragment fragment) {
        add(fragment, false, null);
    }

    public void add(Fragment fragment, boolean isToAddBack) {
        add(fragment, isToAddBack, null);
    }

    public void add(Fragment fragment, boolean isToAddBack, Bundle bundle) {
        try {
            if (frameParent == null) {
                Toast.makeText(applicationSingle, "No Parant Attached to FlowOrganizer", Toast.LENGTH_SHORT).show();
                return;
            }
            hideSoftKeyboard();
            FragmentManager fm = mainActivity.getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            fragment.setArguments(bundle);
            ft.add(frameParent.getId(), fragment);
            if (isToAddBack)
                ft.addToBackStack(fragment.getClass().getName());
            else
                clearBackStack();
            ft.commit();
        } catch (Exception e) {
        }
    }

    public void replace(Fragment fragment) {
        replace(fragment, false, null);
    }

    public void replace(Fragment fragment, boolean isToAddBack) {
        replace(fragment, isToAddBack, null);
    }

    public void replace(Fragment fragment, boolean isToAddBack, Bundle bundle) {
        if (frameParent == null) {
            Toast.makeText(applicationSingle, "No Parant Attached to FlowOrganizer", Toast.LENGTH_SHORT).show();
            return;
        }
        hideSoftKeyboard();
        FragmentManager fm = mainActivity.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        fragment.setArguments(bundle);
        ft.replace(frameParent.getId(), fragment);
        if (isToAddBack)
            ft.addToBackStack(fragment.getClass().getName());
        else
            clearBackStack();
        ft.commit();
    }

    public boolean hasNoMoreBacks() {
        FragmentManager fm = mainActivity.getSupportFragmentManager();
        int count = fm.getBackStackEntryCount();
        if (count < 1)
            return true;
        else
            return false;
    }

    public void clearBackStack() {
        FragmentManager fm = mainActivity.getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    public void popUpBackToMain() {
        FragmentManager fm = mainActivity.getSupportFragmentManager();
        int size = fm.getBackStackEntryCount();
        for (int i = 0; i < size; i++) {
            fm.popBackStack();
        }
    }

    public void popUpBackTo(int skipNoOfFragment) {
        FragmentManager fm = mainActivity.getSupportFragmentManager();
        int size = fm.getBackStackEntryCount();
        if (skipNoOfFragment > size)
            return;
        else
            size = skipNoOfFragment;
        for (int i = 0; i < size; ++i) {
            fm.popBackStack();
        }
    }

    public void hideSoftKeyboard() {
        try {
            if (mainActivity.getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) mainActivity.getSystemService(mainActivity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(mainActivity.getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void popUpBackTo(Fragment fragment) {
        FragmentManager fm = mainActivity.getSupportFragmentManager();
        fm.popBackStack(fragment.getClass().getName(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

}
