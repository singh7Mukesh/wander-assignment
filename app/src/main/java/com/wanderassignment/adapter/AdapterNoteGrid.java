package com.wanderassignment.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wanderassignment.R;
import com.wanderassignment.databinding.AdapterNotesGridBinding;
import com.wanderassignment.listener.offline.OnNoteClickListener;
import com.wanderassignment.model.offline.Note;

import java.util.List;

/**
 * Created by Mukesh on 25-07-2018.
 */
public class AdapterNoteGrid extends RecyclerView.Adapter<AdapterNoteGrid.ViewHolder> {

    private List<Note> listData;
    private OnNoteClickListener listener;

    public AdapterNoteGrid(List<Note> listData) {
        this.listData = listData;
    }

    public void setOnClickListener(OnNoteClickListener listener) {
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public AdapterNotesGridBinding binding;

        public ViewHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onNoteClick(getAdapterPosition(), listData.get(getAdapterPosition()));
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_notes_grid, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        Note data = listData.get(position);
        viewHolder.binding.setNote(data);
    }

}