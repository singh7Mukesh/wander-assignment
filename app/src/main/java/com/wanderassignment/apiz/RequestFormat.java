package com.wanderassignment.apiz;

import com.google.gson.JsonObject;
import com.wanderassignment.constant.AppConstant;
import com.wanderassignment.model.online.NoteBaseModel;

public class RequestFormat {


    public static JsonObject saveNote(String deviceId, NoteBaseModel note) {
        try {
            JsonObject paramObject = new JsonObject();
            paramObject.addProperty(AppConstant.IRequestResponse.DEVICE_ID, deviceId);
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty(AppConstant.IRequestResponse.TITLE, note.getTitle());
            jsonObject.addProperty(AppConstant.IRequestResponse.NOTE_DETAIL, note.getNoteDetail());
            jsonObject.addProperty(AppConstant.IRequestResponse.SHOT_DESCRIPTION, note.getShotDescription());
            jsonObject.addProperty(AppConstant.IRequestResponse.TIME_IN_MILES, note.getTimeInMiles());
            paramObject.add(AppConstant.IRequestResponse.NOTE, jsonObject);
            return paramObject;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JsonObject updateNote(String deviceId, NoteBaseModel note) {
        try {
            JsonObject paramObject = new JsonObject();
            paramObject.addProperty(AppConstant.IRequestResponse.DEVICE_ID, deviceId);
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty(AppConstant.IRequestResponse.ID, note.getId());
            jsonObject.addProperty(AppConstant.IRequestResponse.TITLE, note.getTitle());
            jsonObject.addProperty(AppConstant.IRequestResponse.NOTE_DETAIL, note.getNoteDetail());
            jsonObject.addProperty(AppConstant.IRequestResponse.SHOT_DESCRIPTION, note.getShotDescription());
            jsonObject.addProperty(AppConstant.IRequestResponse.TIME_IN_MILES, note.getTimeInMiles());
            paramObject.add(AppConstant.IRequestResponse.NOTE, jsonObject);
            return paramObject;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
