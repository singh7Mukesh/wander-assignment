package com.wanderassignment.apiz;

import com.google.gson.JsonObject;
import com.wanderassignment.model.online.BaseModel;
import com.wanderassignment.model.online.NoteResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("getNote/")
    Call<NoteResponse> getNote(@Field("device_id") String deviceId);

    @Headers("Content-Type: application/json")
    @POST("saveNote/?")
    Call<BaseModel> saveNote(@Body JsonObject body);

    @Headers("Content-Type: application/json")
    @POST("updateNote/?")
    Call<BaseModel> updateNote(@Body JsonObject body);


    @FormUrlEncoded
    @POST("getNote/")
    Call<BaseModel> removeNote(@Field("note_id") int noteId);
}