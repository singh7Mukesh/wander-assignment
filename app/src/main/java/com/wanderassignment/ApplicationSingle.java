package com.wanderassignment;


import android.app.Application;

import com.wanderassignment.MainActivity;
import com.wanderassignment.dagger.component.ApplicationComponent;
import com.wanderassignment.dagger.component.DaggerApplicationComponent;
import com.wanderassignment.dagger.module.ActivityModule;
import com.wanderassignment.dagger.module.ApplicationModule;
import com.wanderassignment.dagger.module.FragmentModule;
import com.wanderassignment.dagger.module.NetworkModule;
import com.wanderassignment.dagger.module.RoomModule;


/**
 * Created by Mukesh on 24-07-2018.
 */

public class ApplicationSingle extends Application {

    ApplicationComponent applicationComponent;


    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void init(MainActivity mainActivity) {
        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .roomModule(new RoomModule())
                .networkModule(new NetworkModule())
                .activityModule(new ActivityModule(mainActivity))
                .fragmentModule(new FragmentModule())
                .build();

        applicationComponent.inject(this);

    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

//    public MainActivity getActivity() {
//        return mainActivity;
//    }

//    public void initActivity(MainActivity mainActivity) {
//        this.mainActivity = mainActivity;
//    }

}
