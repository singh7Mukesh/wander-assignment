package com.wanderassignment.constant;

public class AppConstant {

    public interface IAppUtils {
        String DB_NAME = "WanderNotes";
        int SHOT_DESC_LENGTH = 100;
        String TIME_FORMAT_LONG = "dd/MM/yyyy hh:mm aaa";
        String TIME_FORMAT_SHORT = "dd.MMMM hh:mm aaa";
        String APP_LOG_TAG = "Wander_Logs";
    }

    public interface IAppUrls {
        String BASE_URL = "192.168.12.4/base/";
    }

    public interface IRequestResponse {
        String ID = "id";
        String TITLE = "title";
        String NOTE_DETAIL = "noteDetail";
        String TIME_IN_MILES = "timeInMiles";
        String SHOT_DESCRIPTION = "shotDescription";
        String NOTE = "note";
        String DEVICE_ID = "deviceID";
    }

    public interface IHardCodeValues {
        String ANDRID_DEVICE_ID = "sdflsuf212u3f9sdnj";
    }
}
