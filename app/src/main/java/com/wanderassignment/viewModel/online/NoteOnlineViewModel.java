package com.wanderassignment.viewModel.online;

import android.arch.lifecycle.ViewModel;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;

import com.wanderassignment.ApplicationSingle;
import com.wanderassignment.MainActivity;
import com.wanderassignment.apiz.ApiInterface;
import com.wanderassignment.apiz.RequestFormat;
import com.wanderassignment.listener.online.OnLiveNoteSaveListener;
import com.wanderassignment.model.online.BaseModel;
import com.wanderassignment.model.online.NoteBaseModel;
import com.wanderassignment.support.Log;

import java.util.Calendar;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.wanderassignment.constant.AppConstant.IAppUtils.APP_LOG_TAG;
import static com.wanderassignment.constant.AppConstant.IAppUtils.SHOT_DESC_LENGTH;
import static com.wanderassignment.constant.AppConstant.IHardCodeValues.ANDRID_DEVICE_ID;

public class NoteOnlineViewModel extends ViewModel {

    public NoteBaseModel note;
    private OnLiveNoteSaveListener noteSaveListener;

    @Inject
    ApiInterface apiInterface;

    @Inject
    MainActivity mainActivity;


    public NoteOnlineViewModel(ApplicationSingle application) {
        application.getApplicationComponent().inject(this);
    }

    public void registerCallBack(OnLiveNoteSaveListener noteSaveListener) {
        this.noteSaveListener = noteSaveListener;
    }

    public void init(NoteBaseModel note) {
        this.note = note;
    }


    public TextWatcher getTitleTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (note == null)
                    note = new NoteBaseModel();
                note.setTitle(s.toString());
                if (!TextUtils.isEmpty(s.toString())) {
                    if (noteSaveListener != null)
                        noteSaveListener.onRefreshDoneView(View.VISIBLE);
                } else canRemoveNote();
            }
        };
    }

    public TextWatcher getDetailTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (note == null)
                    note = new NoteBaseModel();
                note.setNoteDetail(s.toString());
                if (!TextUtils.isEmpty(s.toString())) {
                    if (noteSaveListener != null)
                        noteSaveListener.onRefreshDoneView(View.VISIBLE);
                } else canRemoveNote();
            }
        };
    }

    private boolean canRemoveNote() {
        boolean canRemoveNote;
        if (note != null) {
            if (TextUtils.isEmpty(note.getTitle()) || TextUtils.isEmpty(note.getTitle().trim())) {
                if (TextUtils.isEmpty(note.getNoteDetail()) || TextUtils.isEmpty(note.getNoteDetail().trim()))
                    canRemoveNote = true;
                else
                    return false;
            } else
                return false;
        } else
            canRemoveNote = true;
        if (canRemoveNote) {
            if (noteSaveListener != null)
                noteSaveListener.onRefreshDoneView(View.GONE);
        }
        return canRemoveNote;
    }


    public void done() {
        if (noteSaveListener != null)
            noteSaveListener.onDone();
    }

    public void processNote() {
        if (canRemoveNote()) {
            if (note != null && note.getId() > 0)
                deleteNote();
            return;
        }
        boolean isToUpdateNote = false;
        if (note != null) {
            note.setTimeInMiles(Calendar.getInstance().getTimeInMillis());
            String description = note.getNoteDetail();
            if (!TextUtils.isEmpty(description)) {
                String shortDescription;
                if (description.length() > SHOT_DESC_LENGTH)
                    shortDescription = description.substring(0, SHOT_DESC_LENGTH);
                else
                    shortDescription = description;
                note.setShotDescription(shortDescription);
            } else
                note.setShotDescription("");
        }

        if (note.getId() != 0)
            isToUpdateNote = true;

        if (isToUpdateNote) {
            updateNote();
            if (noteSaveListener != null)
                noteSaveListener.onNoteUpdated(note);
        } else {
            saveNote();
            if (noteSaveListener != null)
                noteSaveListener.onNewNoteAdded(note);
        }
    }

    private void updateNote() {
        mainActivity.loading(true);
        Call<BaseModel> call = apiInterface.updateNote(RequestFormat.updateNote(
                ANDRID_DEVICE_ID, note));

        call.enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                mainActivity.loading(false);
                if (response == null || response.body() == null) {
                    Log.e(APP_LOG_TAG, "No Response From Server");
                    return;
                }
                String message = response.body().getMessage();
                int code = response.body().getCode();
                if (code == 200) {
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                mainActivity.loading(false);
                Log.e(APP_LOG_TAG, "Loading Fail, Please Try again");
            }
        });
    }

    private void saveNote() {
        mainActivity.loading(true);
        Call<BaseModel> call = apiInterface.saveNote(RequestFormat.saveNote(
                ANDRID_DEVICE_ID, note));

        call.enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                mainActivity.loading(false);
                if (response == null || response.body() == null) {
                    Log.e(APP_LOG_TAG, "No Response From Server");
                    return;
                }
                String message = response.body().getMessage();
                int code = response.body().getCode();
                if (code == 200) {
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                mainActivity.loading(false);
                Log.e(APP_LOG_TAG, "Loading Fail, Please Try again");
            }
        });
    }

    private void deleteNote() {
        mainActivity.loading(true);
        Call<BaseModel> call = apiInterface.removeNote(note.getId());

        call.enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                mainActivity.loading(false);
                if (response == null || response.body() == null) {
                    Log.e(APP_LOG_TAG, "No Response From Server");
                    return;
                }
                String message = response.body().getMessage();
                int code = response.body().getCode();
                if (code == 200) {
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                mainActivity.loading(false);
                Log.e(APP_LOG_TAG, "Loading Fail, Please Try again");
            }
        });
    }
}
