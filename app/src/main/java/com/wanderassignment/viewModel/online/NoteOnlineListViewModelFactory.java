package com.wanderassignment.viewModel.online;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.wanderassignment.ApplicationSingle;
import com.wanderassignment.viewModel.offline.NoteListViewModel;


public class NoteOnlineListViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    ApplicationSingle applicationSingle;

    public NoteOnlineListViewModelFactory(ApplicationSingle applicationSingle) {
        this.applicationSingle = applicationSingle;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(NoteOnlineListViewModel.class)) {
            return (T) new NoteOnlineListViewModel(applicationSingle);
        }
        throw new IllegalArgumentException("Unknown class name");
    }
}