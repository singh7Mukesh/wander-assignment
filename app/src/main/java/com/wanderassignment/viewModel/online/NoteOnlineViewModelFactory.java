package com.wanderassignment.viewModel.online;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.wanderassignment.ApplicationSingle;
import com.wanderassignment.viewModel.offline.NoteViewModel;


public class NoteOnlineViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    ApplicationSingle applicationSingle;

    public NoteOnlineViewModelFactory(ApplicationSingle applicationSingle) {
        this.applicationSingle = applicationSingle;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(NoteOnlineViewModel.class)) {
            return (T) new NoteOnlineViewModel(applicationSingle);
        }
        throw new IllegalArgumentException("Unknown class name");
    }
}