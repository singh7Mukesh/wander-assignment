package com.wanderassignment.viewModel.online;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.wanderassignment.MainActivity;
import com.wanderassignment.apiz.ApiInterface;
import com.wanderassignment.db.AppDatabase;
import com.wanderassignment.listener.OnCreateNewNoteListener;
import com.wanderassignment.model.online.NoteBaseModel;
import com.wanderassignment.model.online.NoteResponse;
import com.wanderassignment.ApplicationSingle;
import com.wanderassignment.support.Log;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.wanderassignment.constant.AppConstant.IAppUtils.APP_LOG_TAG;
import static com.wanderassignment.constant.AppConstant.IHardCodeValues.ANDRID_DEVICE_ID;

public class NoteOnlineListViewModel extends ViewModel {

    private MutableLiveData<List<NoteBaseModel>> listMutableLiveData = new MutableLiveData<>();
    private int selectedNotePosition = -1;

    private List<NoteBaseModel> listNotes;

    private final MutableLiveData<NoteBaseModel> selected = new MutableLiveData();

    private OnCreateNewNoteListener onCreateNewNoteListener;

    public void select(NoteBaseModel item) {
        selected.setValue(item);
    }

    @Inject
    ApiInterface apiInterface;

    @Inject
    MainActivity mainActivity;

    public LiveData<NoteBaseModel> getSelected() {
        return selected;
    }

    public void registerCallBack(OnCreateNewNoteListener onCreateNewNoteListener) {
        this.onCreateNewNoteListener = onCreateNewNoteListener;
    }

    public NoteOnlineListViewModel(ApplicationSingle application) {
        application.getApplicationComponent().inject(this);
    }

    public void removeNote() {
        List<NoteBaseModel> clonedList = new ArrayList<>(listNotes.size());
        clonedList.addAll(listNotes);

        if (selectedNotePosition != -1) {
            clonedList.remove(selectedNotePosition);
        }
        listMutableLiveData.setValue(clonedList);
        listNotes = clonedList;
    }

    public void updateMutableListData(NoteBaseModel note) {
        ArrayList<NoteBaseModel> clonedList = new ArrayList<>(listNotes.size());
        clonedList.addAll(listNotes);

        if (selectedNotePosition != -1) {
            clonedList.remove(selectedNotePosition);
            clonedList.add(0, note);
        } else
            clonedList.add(0, note);
        listMutableLiveData.setValue(clonedList);
        listNotes = clonedList;
    }

    private void getNoteList() {
        mainActivity.loading(true);
        Call<NoteResponse> call = apiInterface.getNote(ANDRID_DEVICE_ID);

        call.enqueue(new Callback<NoteResponse>() {
            @Override
            public void onResponse(Call<NoteResponse> call, Response<NoteResponse> response) {
                mainActivity.loading(false);
                if (response == null || response.body() == null) {
                    Log.e(APP_LOG_TAG, "No Response From Server");
                    return;
                }
                String message = response.body().getMessage();
                int code = response.body().getCode();
                if (code == 200) {
                    listNotes = response.body().getListNotes();
                    if (listNotes != null && listNotes.size() > 0)
                        listMutableLiveData.setValue(listNotes);
                }
            }

            @Override
            public void onFailure(Call<NoteResponse> call, Throwable t) {
                mainActivity.loading(false);
                Log.e(APP_LOG_TAG, "Loading Fail, Please Try again");
            }
        });
    }


    public MutableLiveData<List<NoteBaseModel>> getListMutableLiveData() {
        getNoteList();
        return listMutableLiveData;
    }

    public void setSelectedNotePosition(int selectedNotePosition) {
        this.selectedNotePosition = selectedNotePosition;
    }

    public void addNote() {
        if (onCreateNewNoteListener != null)
            onCreateNewNoteListener.onAddNewNote();
    }
}
