package com.wanderassignment.viewModel.offline;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.wanderassignment.ApplicationSingle;


public class NoteViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    ApplicationSingle applicationSingle;

    public NoteViewModelFactory(ApplicationSingle applicationSingle) {
        this.applicationSingle = applicationSingle;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(NoteViewModel.class)) {
            return (T) new NoteViewModel(applicationSingle);
        }
        throw new IllegalArgumentException("Unknown class name");
    }
}