package com.wanderassignment.viewModel.offline;

import android.arch.lifecycle.ViewModel;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;

import com.wanderassignment.db.AppDatabase;
import com.wanderassignment.listener.offline.OnNoteSaveListener;
import com.wanderassignment.model.offline.Note;
import com.wanderassignment.model.offline.NoteDetail;
import com.wanderassignment.ApplicationSingle;

import java.util.Calendar;

import javax.inject.Inject;

import static com.wanderassignment.constant.AppConstant.IAppUtils.SHOT_DESC_LENGTH;

public class NoteViewModel extends ViewModel {

    public Note note;
    public NoteDetail noteDetail;
    private OnNoteSaveListener noteSaveListener;
    @Inject
    public AppDatabase database;


    public NoteViewModel(ApplicationSingle application) {
        application.getApplicationComponent().inject(this);
    }

    public void registerCallBack(OnNoteSaveListener noteSaveListener) {
        this.noteSaveListener = noteSaveListener;
    }

    public void init(Note note, NoteDetail noteDetail) {
        this.note = note;
        this.noteDetail = noteDetail;
    }


    public TextWatcher getTitleTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (note == null)
                    note = new Note();
                note.setTitle(s.toString());
                if (!TextUtils.isEmpty(s.toString())) {
                    if (noteSaveListener != null)
                        noteSaveListener.onRefreshDoneView(View.VISIBLE);
                } else canRemoveNote();
            }
        };
    }

    public TextWatcher getDetailTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (noteDetail == null)
                    noteDetail = new NoteDetail();
                noteDetail.setDescription(s.toString());
                if (!TextUtils.isEmpty(s.toString())) {
                    if (noteSaveListener != null)
                        noteSaveListener.onRefreshDoneView(View.VISIBLE);
                } else canRemoveNote();
            }
        };
    }

    private boolean canRemoveNote() {
        boolean canRemoveNote;
        if (note != null) {
            if (TextUtils.isEmpty(note.getTitle()) || TextUtils.isEmpty(note.getTitle().trim()))
                canRemoveNote = true;
            else
                return false;
        } else
            canRemoveNote = true;
        if (canRemoveNote) {
            if (noteDetail != null) {
                if (TextUtils.isEmpty(noteDetail.getDescription()) || TextUtils.isEmpty(noteDetail.getDescription().trim()))
                    canRemoveNote = true;
                else
                    return false;
            } else
                canRemoveNote = true;
        }
        if (canRemoveNote) {
            if (noteSaveListener != null)
                noteSaveListener.onRefreshDoneView(View.GONE);
        }
        return canRemoveNote;
    }

    private boolean isNeedToUpdate(long nodeId) {
        if (nodeId != 0) {
            Note existingNote = database.getNoteDao().get((int) nodeId);
            NoteDetail existingNoteDetail = database.getNoteDetailDao().get((int) nodeId);
            String existNoteTitle = existingNote.getTitle();
            String changedNoteTitle = note.getTitle();
            if (!TextUtils.isEmpty(changedNoteTitle)) {
                changedNoteTitle = changedNoteTitle.trim();
                note.setTitle(changedNoteTitle);
            }
            if (TextUtils.isEmpty(existNoteTitle) && !TextUtils.isEmpty(changedNoteTitle))
                return true;
            else if (!TextUtils.isEmpty(existNoteTitle) && TextUtils.isEmpty(changedNoteTitle))
                return true;
            else if (!TextUtils.isEmpty(existNoteTitle) && !TextUtils.isEmpty(changedNoteTitle)) {
                if (!existNoteTitle.equals(changedNoteTitle))
                    return true;
            }
            if (noteDetail != null) {
                String existNoteDetail = existingNoteDetail.getDescription();
                String changedNoteDetail = noteDetail.getDescription();

                if (!TextUtils.isEmpty(changedNoteDetail)) {
                    changedNoteDetail = changedNoteDetail.trim();
                    noteDetail.setDescription(changedNoteDetail);
                }
                if (TextUtils.isEmpty(existNoteDetail) && !TextUtils.isEmpty(changedNoteDetail))
                    return true;
                else if (!TextUtils.isEmpty(existNoteDetail) && TextUtils.isEmpty(changedNoteDetail))
                    return true;
                else if (!TextUtils.isEmpty(existNoteDetail) && !TextUtils.isEmpty(changedNoteDetail)) {
                    if (!existNoteDetail.equals(changedNoteDetail))
                        return true;
                }
                return false;
            }
        }
        return true;
    }

    public void done() {
        if (noteSaveListener != null)
            noteSaveListener.onDone();
    }

    public void saveNote(long nodeId) {
        if (canRemoveNote()) {
            if (noteDetail != null && noteDetail.getId() > 0)
                database.getNoteDetailDao().delete(noteDetail);
            if (note != null && note.getId() > 0) {
                database.getNoteDao().delete(note);
                if (noteSaveListener != null)
                    noteSaveListener.onNoteRemove();
            }
            return;
        }
        if (!isNeedToUpdate(nodeId))
            return;
        boolean isToUpdateNote = false, isToUpdateNoteDetail = false;
        if (note != null || noteDetail != null) {
            if (note == null)
                note = new Note();
            note.setTimeInMiles(Calendar.getInstance().getTimeInMillis());

            if (noteDetail == null)
                noteDetail = new NoteDetail();
            else {
                if (noteDetail.getId() > 0)
                    isToUpdateNoteDetail = true;
                String description = noteDetail.getDescription();
                if (!TextUtils.isEmpty(description)) {
                    String shortDescription;
                    if (description.length() > SHOT_DESC_LENGTH)
                        shortDescription = description.substring(0, SHOT_DESC_LENGTH);
                    else
                        shortDescription = description;
                    note.setShotDescription(shortDescription);
                } else
                    note.setShotDescription("");
            }

            if (nodeId != 0)
                isToUpdateNote = true;

            if (isToUpdateNote) {
                database.getNoteDao().update(note);
                if (noteSaveListener != null)
                    noteSaveListener.onNoteUpdated(note);
            } else {
                nodeId = database.getNoteDao().insert(note);
                if (noteSaveListener != null)
                    noteSaveListener.onNewNoteAdded(note);
            }
            if (isToUpdateNoteDetail)
                database.getNoteDetailDao().update(noteDetail);
            else {
                noteDetail.setNoteId((int) nodeId);
                database.getNoteDetailDao().insert(noteDetail);
            }
        }
    }
}
