package com.wanderassignment.viewModel.offline;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.wanderassignment.ApplicationSingle;


public class NoteListViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    ApplicationSingle applicationSingle;

    public NoteListViewModelFactory(ApplicationSingle applicationSingle) {
        this.applicationSingle = applicationSingle;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(NoteListViewModel.class)) {
            return (T) new NoteListViewModel(applicationSingle);
        }
        throw new IllegalArgumentException("Unknown class name");
    }
}