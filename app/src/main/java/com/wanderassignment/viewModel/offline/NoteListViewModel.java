package com.wanderassignment.viewModel.offline;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.wanderassignment.db.AppDatabase;
import com.wanderassignment.listener.OnCreateNewNoteListener;
import com.wanderassignment.model.offline.Note;
import com.wanderassignment.ApplicationSingle;

import java.util.ArrayList;

import javax.inject.Inject;

public class NoteListViewModel extends ViewModel {

    private MutableLiveData<ArrayList<Note>> listMutableLiveData = new MutableLiveData<>();
    private int selectedNotePosition = -1;

    private ArrayList<Note> listNotes;

    private final MutableLiveData<Note> selected = new MutableLiveData();

    private OnCreateNewNoteListener onCreateNewNoteListener;

    public void select(Note item) {
        selected.setValue(item);
    }

    @Inject
    public AppDatabase database;

    public LiveData<Note> getSelected() {
        return selected;
    }

    public void registerCallBack(OnCreateNewNoteListener onCreateNewNoteListener) {
        this.onCreateNewNoteListener = onCreateNewNoteListener;
    }

    public NoteListViewModel(ApplicationSingle application) {
        application.getApplicationComponent().inject(this);
    }

    public void removeNote() {
        ArrayList<Note> clonedList = new ArrayList<>(listNotes.size());
        clonedList.addAll(listNotes);

        if (selectedNotePosition != -1) {
            clonedList.remove(selectedNotePosition);
        }
        listMutableLiveData.setValue(clonedList);
        listNotes = clonedList;
    }

    public void updateMutableListData(Note note) {
        ArrayList<Note> clonedList = new ArrayList<>(listNotes.size());
        clonedList.addAll(listNotes);

        if (selectedNotePosition != -1) {
            clonedList.remove(selectedNotePosition);
            clonedList.add(0, note);
        } else
            clonedList.add(0, note);
        listMutableLiveData.setValue(clonedList);
        listNotes = clonedList;
    }


    public MutableLiveData<ArrayList<Note>> getListMutableLiveData() {
        listNotes = (ArrayList<Note>) database.getNoteDao().getAll();

        listMutableLiveData.setValue(listNotes);
        return listMutableLiveData;
    }

    public void setSelectedNotePosition(int selectedNotePosition) {
        this.selectedNotePosition = selectedNotePosition;
    }

    public void addNote() {
        if (onCreateNewNoteListener != null)
            onCreateNewNoteListener.onAddNewNote();
    }
}
